const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SpielerSchema = new Schema({
  nachname: {
    type: String,
    required: true
  },
  vorname: {
    type: String,
    required: true
  },
  geboren: {
    type: String,
    required: true
  }
});

SpielerSchema.index({ nachname: 1, vorname: 1, geboren: 1 }, { unique: true, sparse: true });

module.exports = Spieler = mongoose.model('spieler', SpielerSchema);
