const express = require('express');
const mongoose = require('mongoose');

const app = express();

app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));

// Connect to MongoDB
mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB verbunden'))
  .catch(err => console.log('MongoDB Verbindungsfehler: ', err));

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Bei MongoDB abmelden beim Beenden der App');
    process.exit(0);
  });
});

const Spieler = require('./models/spieler');

app.get('/', (req, res) => {
  Spieler.find().sort({ "nachname": 1 })
    .then(spielers => res.render('index', { spielers }))
    .catch(err => res.status(404).json({ msg: 'Keine Spieler gefunden' }));
});

app.post('/spieler/add', (req, res) => {
  if (req.body.nachname && req.body.vorname && req.body.geboren) {
    const newSpieler = new Spieler({
      nachname: req.body.nachname,
      vorname: req.body.vorname,
      geboren: req.body.geboren
    });

    newSpieler.save().then(spieler => res.redirect('/'));
  } else {
    res.redirect('/');
  }

});

app.post('/spieler/delete/:id', (req, res) => {
  Spieler.findByIdAndRemove(req.params.id).then(result => res.redirect('/'));
});

const port = 3000;

app.listen(port, () => console.log('Server gestartet...'));
